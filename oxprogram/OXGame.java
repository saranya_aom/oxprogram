import java.util.Scanner;

public class OXGame {
	public static void main(String[] args) {

		char ch[][] = new char[3][3];
		Boolean result = false;
		int round = 1;
		char ox;

		createTable(ch);

		showTable(ch);

		// input and check
		do {
			input(ch,round);

			// check
			for (int i = 0; i < 3; i++) {

				for (int j = 0; j < 3; j++) {
					result = owin(ch, result);
					if (result != true)
						result = xwin(ch, result);

					System.out.print(ch[i][j] + " | ");
				}
				System.out.println();
			}

			System.out.println();

			round++;

		} while (result != true && round <= 9);

		// X or O 's win
		if (round % 2 == 0 && round <= 9)
			ox = 'x';
		else if (round % 2 != 0 && round <= 9)
			ox = 'o';
		else
			ox = 'n';

		// Result of Game
		if (result == true && ox == 'o')
			System.out.println(oresult(result));
		else if (result == true && ox == 'x')
			System.out.println(xresult(result));
		else
			System.out.println("Draw!");

	}
	
	public static Boolean owin(char[][] ch, Boolean result) {
		for(int i=0;i<3;i++) {
			if (ch[0][i] == 'o' && ch[1][i] == 'o' && ch[2][i] == 'o')
			return true;
		}
		for(int i=0;i<3;i++) {
			if (ch[i][0] == 'o' && ch[i][1] == 'o' && ch[i][2] == 'o')
			return true;
		}

		if ((ch[0][2] == 'o' && ch[1][1] == 'o' && ch[2][0] == 'o')||(ch[2][0] == 'o' && ch[1][1] == 'o' && ch[0][2] == 'o'))
			return true;
		else if (ch[0][0] == 'o' && ch[2][2] == 'o' && ch[1][1] == 'o')
			return true;
	
		return false;
	}
	
	public static Boolean xwin(char[][] ch, Boolean result) {
		for(int i=0;i<3;i++) {
			if (ch[0][i] == 'x' && ch[1][i] == 'x' && ch[2][i] == 'x')
			return true;
		}
		for(int i=0;i<3;i++) {
			if (ch[i][0] == 'x' && ch[i][1] == 'x' && ch[i][2] == 'x')
			return true;
		}

		if (ch[0][2] == 'x' && ch[1][1] == 'x' && ch[2][0] == 'x')
			return true;
		else if (ch[0][0] == 'x' && ch[2][2] == 'x' && ch[1][1] == 'x')
			return true;
	
		return false;
	}
	
	public static String xresult(Boolean result) {
		return "X win!";
	}

	public static String oresult(Boolean result) {
		return "O win!";
	}
	
	public static void showTable(char[][] ch) {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				System.out.print(ch[i][j] + " | ");
			}
			System.out.println();
		}
		System.out.println();
	}
	
	public static void createTable(char[][] ch) {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				ch[i][j] = '_';
			}
		}
	}
	
	public static void input(char[][] ch,int round) {
		Scanner kb = new Scanner(System.in);
		if (round % 2 != 0) {
			System.out.print("X(R,C) : ");
			int n1 = kb.nextInt();
			int n2 = kb.nextInt();

			if (n1 > 3 || n1 < 1 || n2 > 3 || n2 < 1) {
				System.out.println("Please enter only number 1-3 (1, 2 or 3)");
				System.out.print("X(R,C) : ");
				n1 = kb.nextInt();
				n2 = kb.nextInt();
			}

			if (ch[n1 - 1][n2 - 1] == '_')
				ch[n1 - 1][n2 - 1] = 'x';
			else {
				System.out.println("This position is already input, please try again...");
				System.out.print("X(R,C) : ");
				n1 = kb.nextInt();
				n2 = kb.nextInt();
				ch[n1 - 1][n2 - 1] = 'x';
			}
		} else {
			System.out.print("O(R,C) : ");
			int n1 = kb.nextInt();
			int n2 = kb.nextInt();

			if (n1 > 3 || n1 < 1 || n2 > 3 || n2 < 1) {
				System.out.println("Please enter only number 1-3 (1, 2 or 3)");
				System.out.print("O(R,C) : ");
				n1 = kb.nextInt();
				n2 = kb.nextInt();
			}

			if (ch[n1 - 1][n2 - 1] == '_')
				ch[n1 - 1][n2 - 1] = 'o';
			else {
				System.out.println("This position is already input, please try again...");
				System.out.print("O(R,C) : ");
				n1 = kb.nextInt();
				n2 = kb.nextInt();
				ch[n1 - 1][n2 - 1] = 'o';
			}
		}
	}
}
