public class Xwin {

	public Boolean xwin(char[][] ch, Boolean result) {
		for(int i=0;i<3;i++) {
			if (ch[0][i] == 'x' && ch[1][i] == 'x' && ch[2][i] == 'x')
			return true;
		}
		for(int i=0;i<3;i++) {
			if (ch[i][0] == 'x' && ch[i][1] == 'x' && ch[i][2] == 'x')
			return true;
		}

		if (ch[0][2] == 'x' && ch[1][1] == 'x' && ch[2][0] == 'x')
			return true;
		else if (ch[0][0] == 'x' && ch[2][2] == 'x' && ch[1][1] == 'x')
			return true;
	
		return false;
	}

	public String result(Boolean result) {
		return "X win!";
	}

}
